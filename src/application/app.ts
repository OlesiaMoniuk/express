import cors from 'cors';
import express from 'express';
import logger from './middlewares/logger.middleware';
import studentsRouter from '../students/students.router';
import lectorsRouter from '../lectors/lector.rounter';
import bodyParser from 'body-parser';
import exceptionFilter from './middlewares/exceptions.filter';
import path from 'path';
import { AppDataSource } from '../configs/database/data-source';
import groupsRouter from './groups/groups.router';
import coursesRouter from './courses/courses.router';
import marksRouter from './marks/marks.router';

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(logger);

AppDataSource.initialize()
    .then(() => {
        console.log('Typeorm connected to database');
    })
    .catch((error) => {
        console.log('Error:', error);
    })

const staticFilesPath = path.join(__dirname, '../', 'public');

app.use('/api/v1/public', express.static(staticFilesPath));
app.use('/api/v1/students', studentsRouter);
app.use('/api/v1/lectors', lectorsRouter);
app.use('/api/v1/groups', groupsRouter);
app.use('/api/v1/courses', coursesRouter);
app.use('/api/v1/marks', marksRouter);
app.use(exceptionFilter);

export default app;