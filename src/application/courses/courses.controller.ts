import { NextFunction, Request, Response } from "express";
import * as coursesService from './courses.service';
import { ValidatedRequest } from 'express-joi-validation';
import { ICourseCreateRequest } from "./types/course-create-request.interface";

export const getAllCourses = async (request: Request, response: Response) => {
    const courses = await coursesService.getAllCourses();
    response.json(courses);
};

export const createCourse = async (request: ValidatedRequest<ICourseCreateRequest>, response: Response) => {
    const courses = await coursesService.createCourse(request.body);
    response.json(courses);
};
