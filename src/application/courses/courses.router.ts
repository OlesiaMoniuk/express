import { Router } from "express";
import * as coursesController from './courses.controller';
import { courseCreateSchema, courseUpdateSchema } from "./courses.schema";
import controllerWrapper from "../unilities/controller-wrapper";
import validator from "../middlewares/validation.middleware";

const router = Router();

router.get('/', controllerWrapper(coursesController.getAllCourses));
router.post('/', validator.body(courseCreateSchema), controllerWrapper(coursesController.createCourse));

export default router;