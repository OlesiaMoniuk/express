import Joi from "joi";
import { ICourse } from "./types/course.interface";

export const courseCreateSchema = Joi.object<Omit<ICourse, 'id'>>(
    {
        name: Joi.string().required(),
    }
);

export const courseUpdateSchema = Joi.object<Partial<ICourse>>(
    {
        name: Joi.string().optional(),
    }
);