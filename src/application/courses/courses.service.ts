import { ICourse} from './types/course.interface';
import { Course } from './entities/course.entity';
import { AppDataSource } from '../../configs/database/data-source';
import { HttpStatusCode } from '../enums/http-statuses.enum';
import HttpException from '../exceptions/http-exception';

const coursesRepository = AppDataSource.getRepository(Course);

export const getAllCourses = async (): Promise<Course[]> => {
    return await coursesRepository.find({});
};

export const createCourse = async (createCourseSchema: Omit<ICourse, 'id'>): Promise<Course> => {
    const course = await coursesRepository.findOne({
        where: {
            name: createCourseSchema.name
        }
    });
if (course) {
    throw new HttpException(
        HttpStatusCode.BAD_REQUEST,
        'Course with this name already exist',
    )
}

    return coursesRepository.save(createCourseSchema);
};
