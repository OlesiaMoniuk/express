import { Column, Entity, OneToMany } from "typeorm";
import { CoreEntity } from "../../entities/core.entity";


@Entity()
export class Course extends CoreEntity {
   
    @Column({
        type: 'varchar',
        nullable: true,
    })
    name: string;

}