import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { ICourse } from './course.interface';


export interface ICourseUpdateRequest extends ValidatedRequestSchema {
    [ContainerTypes.Body]: Partial<ICourse>;
}