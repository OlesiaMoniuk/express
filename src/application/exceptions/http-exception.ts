class HttpException extends Error {
    status: number;
    massage: string;
    constructor(status:number, massage: string) {
        super(massage);
        this.status = status;
        this.massage = massage;
    }
}
export default HttpException;