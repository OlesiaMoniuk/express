import { Column, Entity, OneToMany } from "typeorm";
import { CoreEntity } from "../../entities/core.entity";
import { Student } from "../../../students/entities/student.entity";


@Entity()
export class Group extends CoreEntity {
   
    @Column({
        type: 'varchar',
        nullable: true,
    })
    name: string;

    @OneToMany(() => Student, (student) => student.group)
    students: Student[];
}