import { Router } from "express";
import * as groupsController from './groups.controller';
import { groupCreateSchema, groupUpdateSchema } from "./group.schema";
import controllerWrapper from "../unilities/controller-wrapper";
import validator from "../middlewares/validation.middleware";

const router = Router();

router.get('/', controllerWrapper(groupsController.getAllGroups));
router.get('/:id', controllerWrapper(groupsController.getGroupById));
router.post('/', validator.body(groupCreateSchema), controllerWrapper(groupsController.createGroup));
router.patch('/:id', validator.body(groupUpdateSchema), controllerWrapper(groupsController.updateGroupById));
router.delete('/:id', controllerWrapper(groupsController.deleteGroupById));
router.get('/:id', controllerWrapper(groupsController.getGroupWithStudents));

export default router;