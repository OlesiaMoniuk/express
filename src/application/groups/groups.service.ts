import { DeleteResult, UpdateResult } from 'typeorm';
import { IGroup } from './types/group.interface';
import { Group } from './entities/group.entity';
import { AppDataSource } from '../../configs/database/data-source';
import { HttpStatusCode } from '../enums/http-statuses.enum';
import HttpException from '../exceptions/http-exception';

const groupsRepository = AppDataSource.getRepository(Group);

export const getAllGroups = async (): Promise<Group[]> => {
    return await groupsRepository.find({});
};

export const getGroupById = async (id: string) => {
    const group = await  groupsRepository.findOne({
        where: {
            id
        }
    });

    if(!group) {
        throw new HttpException(HttpStatusCode.NOT_FOUND, "Groups is not found");
    }
    return group;
};

export const createGroup = async (createGroupSchema: Omit<IGroup, 'id'>): Promise<Group> => {
    const group = await groupsRepository.findOne({
        where: {
            name: createGroupSchema.name
        }
    });
if (group) {
    throw new HttpException(
        HttpStatusCode.BAD_REQUEST,
        'Group with this name already exist',
    )
}

    return groupsRepository.save(createGroupSchema);
};

export const updateGroupById = async (id: string, groupUpdateSchema: Partial<IGroup>): Promise<UpdateResult> => {
    const group = await groupsRepository.update(id, groupUpdateSchema);
    if (!group) {
        throw new HttpException(
            HttpStatusCode.NOT_FOUND,
            'Group is not found',
        );
    }

    return group;  
};


export const deleteGroupById = async (id: string): Promise<DeleteResult> => {
    const group = await groupsRepository.delete(id);
    if (!group) {
        throw new HttpException(
            HttpStatusCode.NOT_FOUND,
            'Group is not found',
        );
    }
    return group;
};

export const getStudentByGroupId = async (id: string): Promise<Group> => {
    const group = await  groupsRepository
        .createQueryBuilder('group')
        .leftJoinAndSelect('student.group', 'group')
        .where('group.id = :id', { id })
        .getOne();

    if(!group) {
        throw new HttpException(HttpStatusCode.NOT_FOUND, "Student not found");
    }
    return group;
};