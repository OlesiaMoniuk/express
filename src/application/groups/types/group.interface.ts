export interface IGroup {
    id: string,
    name: string,
    student?: string
}