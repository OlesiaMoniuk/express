import { Column, Entity, JoinTable, ManyToMany } from "typeorm";
import { CoreEntity } from "../../entities/core.entity";
import { Student } from "../../../students/entities/student.entity";


@Entity()
export class Mark extends CoreEntity {
   
    @Column({
        type: 'numeric',
        nullable: true,
    })
    grade: number;

    @ManyToMany(() => Student, (student) => student.marks)
    @JoinTable({ name: 'marks'})
    students: Student[]

}