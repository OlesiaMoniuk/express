import { NextFunction, Request, Response } from "express";
import * as marksService from './marks.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IMarkCreateRequest } from "./types/marks-create-request.interface";

export const getAllMarks = async (request: Request, response: Response) => {
    const marks = await marksService.getAllMarks();
    response.json(marks);
};

export const createMark = async (request: ValidatedRequest<IMarkCreateRequest>, response: Response) => {
    const marks = await marksService.createMark(request.body);
    response.json(marks);
};
