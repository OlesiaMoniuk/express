import { Router } from "express";
import * as marksController from './marks.controller';
import { markCreateSchema } from "./marks.schema";
import controllerWrapper from "../unilities/controller-wrapper";
import validator from "../middlewares/validation.middleware";

const router = Router();

router.get('/', controllerWrapper(marksController.getAllMarks));
router.post('/', validator.body(markCreateSchema), controllerWrapper(marksController.createMark));

export default router;