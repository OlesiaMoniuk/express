import Joi from "joi";
import { IMark } from "../marks/types/marks.interface";

export const markCreateSchema = Joi.object<Omit<IMark, 'id'>>(
    {
        grade: Joi.number().required(),
    }
);

export const markUpdateSchema = Joi.object<Partial<IMark>>(
    {
        grade: Joi.number().optional(),
    }
);