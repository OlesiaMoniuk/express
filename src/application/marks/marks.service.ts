import { IMark} from '../marks/types/marks.interface';
import { Mark } from '../marks/entities/marks.entity';
import { AppDataSource } from '../../configs/database/data-source';
import { HttpStatusCode } from '../enums/http-statuses.enum';
import HttpException from '../exceptions/http-exception';

const marksRepository = AppDataSource.getRepository(Mark);

export const getAllMarks = async (): Promise<Mark[]> => {
    return await marksRepository.find({});
};

export const createMark = async (createMarkSchema: Omit<IMark, 'id'>): Promise<Mark> => {
    const mark = await marksRepository.findOne({
        where: {
            grade: createMarkSchema.grade
        }
    });
if (mark) {
    throw new HttpException(
        HttpStatusCode.BAD_REQUEST,
        'Mark with this name already exist',
    )
}

    return marksRepository.save(createMarkSchema);
};
