import { Request, Response, NextFunction } from 'express';
import HttpException from '../exceptions/http-exception';
import { HttpStatusCode } from '../enums/http-statuses.enum';

const exceptionFilter = (error: HttpException, request: Request, response: Response, next: NextFunction) => {
    const status = error.status || HttpStatusCode.INTERNAL_SERVER_ERROR;
    const massage = error.massage || "Something went wrong";
    response.status(status).send({status, massage});
};

export default exceptionFilter;