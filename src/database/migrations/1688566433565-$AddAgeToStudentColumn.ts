import { MigrationInterface, QueryRunner } from "typeorm";

export class  $AddAgeToStudentColumn1688566433565 implements MigrationInterface {
    name = ' $AddAgeToStudentColumn1688566433565'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "student" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "name" character varying, "surname" character varying, "email" character varying, "age" numeric, "status" "public"."student_status_enum" NOT NULL DEFAULT 'ACTIVE', CONSTRAINT "PK_3d8016e1cb58429474a3c041904" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "student"`);
    }

}
