import { MigrationInterface, QueryRunner } from "typeorm";

export class  $AddSalaryToStudentTable1688566516043 implements MigrationInterface {
    name = ' $AddSalaryToStudentTable1688566516043'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "student" ADD "salary" numeric`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "student" DROP COLUMN "salary"`);
    }

}
