import { Column, Entity } from "typeorm";
import { CoreEntity } from "../../application/entities/core.entity";
import { Status } from "../../students/enums/status.enum";


@Entity()
export class Lector extends CoreEntity {
   
    @Column({
        type: 'varchar',
        nullable: true,
    })
    name: string;
    @Column({
        type: 'varchar',
        nullable: true,
    })
    surname: string;
    @Column({
        type: 'varchar',
        nullable: true,
    })
    email: string;
    @Column({
        type: 'numeric',
        nullable: true,
    })
    age: number;
    @Column({
        type: 'numeric',
        nullable: true,
    })
    salary: number;
    @Column({
        type: 'enum',
        enum: Status,
        default: Status.ACTIVE,
        nullable: false,
    })
    status: Status;
}