import { NextFunction, Request, Response } from "express";
import * as lectorService from '../lectors/lector.service';
import { ValidatedRequest } from 'express-joi-validation';
import { ILectorUpdateRequest } from "./types/lector-update-request.interface";
import { ILectorCreateRequest } from "./types/lector-create-request.interface";

export const getAllLectors = async (request: Request, response: Response) => {
    const lectors = await lectorService.getAllLectors();
    response.json(lectors);
};

export const getLectorById = async (request: Request<{id: string}>, response: Response, next: NextFunction) => {
    const {id} = request.params;
    const lector = await lectorService.getLectorById(id);
    response.json(lector);
};

export const createLector = async (request: ValidatedRequest<ILectorCreateRequest>, response: Response) => {
    const lector = await lectorService.createLector(request.body);
    response.json(lector);
};

export const updateLectorById = async (request: ValidatedRequest<ILectorUpdateRequest>, response: Response) => {
    const {id} = request.params;
    const lector = await lectorService.updateLectorById(id, request.body);
    response.json(lector);
};

export const deleteLectorById = async (request: Request<{id: string}>, response: Response) => {
    const {id} = request.params;
    const lector = await lectorService.deleteLectorById(id);
    response.json(lector);
};
