import { Router } from "express";
import * as lectorController from './lector.controller';
import controllerWrapper from "../application/unilities/controller-wrapper";
import validator from "../application/middlewares/validation.middleware";
import { lectorCreateSchema, lectorUpdateSchema } from "./lector.schema";

const router = Router();


router.get('/', controllerWrapper(lectorController.getAllLectors));
router.get('/:id',controllerWrapper(lectorController.getLectorById));
router.post('/', validator.body(lectorCreateSchema), controllerWrapper(lectorController.createLector));
router.patch('/:id',validator.body(lectorUpdateSchema), controllerWrapper(lectorController.updateLectorById));
router.delete('/:id', controllerWrapper(lectorController.deleteLectorById));

export default router;