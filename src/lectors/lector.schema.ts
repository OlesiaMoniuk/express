import Joi from "joi";
import { ILector } from "./types/lector.interface";

export const lectorCreateSchema = Joi.object<Omit<ILector, 'id'>>(
    {
        name: Joi.string().required(),
        surname: Joi.string().required(),
        email: Joi.string().required(),
        age: Joi.number().required(),
        salary: Joi.number().required(),
    }
);

export const lectorUpdateSchema = Joi.object<Partial<ILector>>(
    {
        name: Joi.string().optional(),
        surname: Joi.string().optional(),
        email: Joi.string().optional(),
        age: Joi.number().optional(),
        salary: Joi.number().optional(),
    }
);