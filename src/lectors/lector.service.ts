import HttpException from '../application/exceptions/http-exception';
import { HttpStatusCode } from '../application/enums/http-statuses.enum';
import { ILector } from './types/lector.interface';
import { AppDataSource } from '../configs/database/data-source';
import { DeleteResult, UpdateResult } from 'typeorm';
import { Lector } from './entities/lector.entity';

const lectorsRepository = AppDataSource.getRepository(Lector);

export const getAllLectors = async (): Promise<Lector[]> => {
    return await lectorsRepository.find({});
};

export const getLectorById = async (id: string) => {
    const lector = await  lectorsRepository.findOne({
        where: {
            id
        }
    });

    if(!lector) {
        throw new HttpException(HttpStatusCode.NOT_FOUND, "Lector not found");
    }
    return lector;
};

export const createLector = async (createLectorSchema: Omit<ILector, 'id'>): Promise<Lector> => {
    const lector = await lectorsRepository.findOne({
        where: {
            email: createLectorSchema.email
        }
    });
if (lector) {
    throw new HttpException(
        HttpStatusCode.BAD_REQUEST,
        'Lector with this email already exist',
    )
}

    return lectorsRepository.save(createLectorSchema);
};

export const updateLectorById = async (id: string, lectorUpdateSchema: Partial<ILector>): Promise<UpdateResult> => {
    const lector = await lectorsRepository.update(id, lectorUpdateSchema);
    if (!lector) {
        throw new HttpException(
            HttpStatusCode.NOT_FOUND,
            'Lector is not found',
        );
    }

    return lector;  
};

export const deleteLectorById = async (id: string): Promise<DeleteResult> => {
    const lector = await lectorsRepository.delete(id);
    if (!lector) {
        throw new HttpException(
            HttpStatusCode.NOT_FOUND,
            'Lector is not found',
        );
    }
    return lector;
};
