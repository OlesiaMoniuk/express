import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { ILector } from './lector.interface';

export interface ILectorUpdateRequest extends ValidatedRequestSchema {
    [ContainerTypes.Body]: Partial<ILector>;
}