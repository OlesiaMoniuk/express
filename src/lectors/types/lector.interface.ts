import { Status } from "../../students/enums/status.enum";

export interface ILector {
    id: string,
    name: string,
    surname: string,
    email: string,
    age: number,
    salary: number,
    status: Status,
    imagePath?: string,
    groupId?: string,
    groupName?: string,
}