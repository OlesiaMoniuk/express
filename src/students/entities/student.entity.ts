import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne } from "typeorm";
import { CoreEntity } from "../../application/entities/core.entity";
import { Status } from "../enums/status.enum";
import { Group } from "../../application/groups/entities/group.entity";
import { Mark } from "../../application/marks/entities/marks.entity";


@Entity()
export class Student extends CoreEntity {
   
    @Column({
        type: 'varchar',
        nullable: true,
    })
    name: string;
    @Column({
        type: 'varchar',
        nullable: true,
    })
    surname: string;
    @Column({
        type: 'varchar',
        nullable: true,
    })
    email: string;
    @Column({
        type: 'numeric',
        nullable: true,
    })
    age: number;
    @Column({
        type: 'numeric',
        nullable: true,
    })
    salary: number;
    @Column({
        type: 'enum',
        enum: Status,
        default: Status.ACTIVE,
        nullable: false,
    })
    status: Status;

    @ManyToOne(() => Group,(group) => group.students, {
        nullable: false,
        eager: false,
    })
    @JoinColumn({ name: 'group_id', referencedColumnName: 'id'})
    group: Group;
    @Column({
        type: 'integer',
        nullable: false,
        name: 'group_id'
    })
    groupId: number;

    @ManyToMany(() => Mark)
    @JoinTable()
    marks: Mark[]
}
