import { NextFunction, Request, Response } from "express";
import * as studentsService from './students.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IStudentUpdateRequest } from "./types/student-update-request.interface";
import { IStudentCreateRequest } from "./types/student-create-request.interface";

export const getAllStudents = async (request: Request, response: Response) => {
    const students = await studentsService.getAllStudents();
    response.json(students);
};

export const getStudentById = async (request: Request<{id: string}>, response: Response, next: NextFunction) => {
    const {id} = request.params;
    const student = await studentsService.getStudentById(id);
    response.json(student);
};

export const createStudent = async (request: ValidatedRequest<IStudentCreateRequest>, response: Response) => {
    const student = await studentsService.createStudent(request.body);
    response.json(student);
};

export const updateStudentById = async (request: ValidatedRequest<IStudentUpdateRequest>, response: Response) => {
    const {id} = request.params;
    const student = await studentsService.updateStudentById(id, request.body);
    response.json(student);
};

export const addImage = (request: Request<{id: string, file: Express.Multer.File}>, response: Response) => {
    const {id} = request.params;
    const {path} = request.file ?? {};
    const student = studentsService.addImage(id, path);
    response.json(student);
};

export const deleteStudentById = async (request: Request<{id: string}>, response: Response) => {
    const {id} = request.params;
    const student = await studentsService.deleteStudentById(id);
    response.json(student);
};

// export const getAllGroups = (request: Request, response: Response) => {
//     const groups = studentsService.getAllGroups();
//     response.json(groups);
// };

// export const getGroupById = (request: Request<{ id: string }>, response: Response, next: NextFunction) => {
//     const { id } = request.params;
//     const group = studentsService.getGroupById(id);
//     response.json(group);
// };

// export const addGroupIdToStudent = (request: Request<{ id: string, groupId: string }>, response: Response) => {
//     const { id, groupId } = request.params;
//     const student = studentsService.addGroupIdToStudent(id, groupId);
//     response.json(student);
// };

// export const createGroup = (request: Request<{ name: string }>, response: Response) => {
//     const { name } = request.body;
//     const group = studentsService.createGroup(name);
//     response.json(group);
// };

// export const updateGroup = (request: Request<{ id: string, name: string }>, response: Response) => {
//     const { id } = request.params;
//     const { name } = request.body;
//     const group = studentsService.updateGroup(id, name);
//     response.json(group);
// };

// export const deleteGroupById = (request: Request<{ id: string }>, response: Response) => {
//     const { id } = request.params;
//     const group = studentsService.deleteGroupById(id);
//     response.json(group);
// };