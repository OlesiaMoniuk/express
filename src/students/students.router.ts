import { Router } from "express";
import * as studentsController from './students.controller';
import controllerWrapper from "../application/unilities/controller-wrapper";
import validator from "../application/middlewares/validation.middleware";
import { studentCreateSchema, studentUpdateSchema } from "./student.schema";
import { idParamsSchema } from "../application/schemas/id-param.chema";
import uploadMiddleware from "../application/middlewares/upload.middleware";

const router = Router();

// validator.params(idParamsSchema),
router.get('/', controllerWrapper(studentsController.getAllStudents));
router.get('/:id',controllerWrapper(studentsController.getStudentById));
router.post('/', validator.body(studentCreateSchema), controllerWrapper(studentsController.createStudent));
router.patch('/:id',validator.body(studentUpdateSchema), controllerWrapper(studentsController.updateStudentById));
// router.patch('/:id/image', uploadMiddleware.single('file'), controllerWrapper(studentsController.addImage));
router.delete('/:id', controllerWrapper(studentsController.deleteStudentById));

export default router;