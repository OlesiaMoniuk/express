import HttpException from '../application/exceptions/http-exception';
import { HttpStatusCode } from '../application/enums/http-statuses.enum';
import { IStudent } from './types/student.interface';
import ObjectID from 'bson-objectid';
import path from 'path';
import fs from 'fs/promises';
import { AppDataSource } from '../configs/database/data-source';
import { Student } from './entities/student.entity';
import { DeleteResult, UpdateResult } from 'typeorm';

const studentsRepository = AppDataSource.getRepository(Student);

export const getAllStudents = async (): Promise<Student[]> => {
    return await studentsRepository.find({});
};

export const getStudentById = async (id: string): Promise<Student> => {
    const student = await  studentsRepository
        .createQueryBuilder('student')
        .select(
            [
                'student.id as is',
                'student.name as name',
                'student.surname as surname',
                'student.email as email',
                'student.age as age',
                'student.salary as salary',
            ]
        )
        .leftJoin('student.group', 'group')
        .addSelect('group.name as "groupName"')
        .where('student.id = :id', { id })
        .getRawOne();

    if(!student) {
        throw new HttpException(HttpStatusCode.NOT_FOUND, "Student not found");
    }
    return student;
};

export const createStudent = async (createStudentSchema: Omit<IStudent, 'id'>,): Promise<Student> => {
    const student = await studentsRepository.findOne({
        where: {
            email: createStudentSchema.email
        }
    });
if (student) {
    throw new HttpException(
        HttpStatusCode.BAD_REQUEST,
        'Student with this email already exist',
    )
}
    return studentsRepository.save({
        ...createStudentSchema,
        groupId: Number(createStudentSchema.groupId),
    });
};

export const updateStudentById = async (id: string, studentUpdateSchema: Partial<IStudent>): Promise<UpdateResult> => {
    const result = await studentsRepository.update(id, studentUpdateSchema as object);
    if (!result.affected) {
        throw new HttpException(
            HttpStatusCode.NOT_FOUND,
            'Student is not found',
        );
    }

    return result;  
};

export const addImage = async (id: string, filePath?: string) => {
    if (!filePath) {
        throw new HttpException(HttpStatusCode.BAD_REQUEST, "File is not provided");
    }
    try {
    const imageId = ObjectID().toHexString();
    const imageExtension = path.extname(filePath);
    const imageName = imageId + imageExtension;

    const studentsDirectoryName = 'students';
    const studentsDirectoryPath = path.join(__dirname, '../', 'public', studentsDirectoryName);
    const newImagePath = path.join(studentsDirectoryPath, imageName);
    const imagePath = `${studentsDirectoryName}/${imageName}`;

    await fs.rename(filePath, newImagePath);

    const updatedStudent = updateStudentById(id, {imagePath});

    return updatedStudent;
    } catch (error) {
        await fs.unlink(filePath);
        throw error;
    } 
};

export const deleteStudentById = async (id: string): Promise<DeleteResult> => {
    const result = await studentsRepository.delete(id);
    if (!result.affected) {
        throw new HttpException(
            HttpStatusCode.NOT_FOUND,
            'Student is not found',
        );
    }
    return result;
};