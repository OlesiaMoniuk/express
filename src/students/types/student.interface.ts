import { Status } from "../enums/status.enum";

export interface IStudent {
    id: string,
    name: string,
    surname: string,
    email: string,
    age: number,
    status: Status,
    imagePath?: string,
    groupId?: string,
    groupName?: string,
}